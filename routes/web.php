<?php

use App\Http\Controllers\ImporterController;

Route::get( '/', function () {
	return view( 'welcome' );
} );

Route::get( '/importJson', function () {
	$controller = new ImporterController();
	$controller->importJson();

	return 'Job created';
} );

Route::get( '/importXml', function () {
	$controller = new ImporterController();
	$controller->importXml();

	return 'Job created';
} );

