<?php

namespace App\Jobs;

use App\Jobs\Joost\Importer\Controller\Importer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportXml implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * Execute the job.
	 *
	 * @return bool
	 */
	public function handle()
	{
		$importer = new Importer( '/../../uploaded/data.xml' );

		return $importer->resumeImportation();
	}
}
