<?php

namespace App\Jobs\Joost\Importer\Model;

class DataStandardization
{

	/**
	 * Convert the field field 'checked' to the Enum 'Yes' and 'No' of database
	 *
	 * @param $data
	 *
	 * @return string
	 */
	public static function checked( $data )
	{
		return (bool)$data ? 'Yes' : 'No';
	}

	/**
	 * Convert a date to a database compatible format
	 *
	 * @param $data
	 *
	 * @return bool|false|string
	 */
	public static function date( $data )
	{
		$date = false;

		/* If we find '/' we assume the format is dd/mm/YYYY (Brazillian)
		 * We could use regex to be sure about the format, but regex is slower and this sample looks to have few variants of date formats, so we opt for a faster alternative.
		 */
		if ( strpos( $data, '/' ) !== false ) {
			$date = strtotime( substr( $data, 6, 4 ) . substr( $data, 3, 2 ) . substr( $data, 0, 2 ) );
		}

		// If is not a brazilian format
		if ( !$date ) {
			$date = strtotime( $data );
		}

		// We were not able to find the original format of the date to convert it or the data is not a date
		if ( !$date ) {
			return false;
		}

		return date( 'Y-m-d', $date );
	}
}