<?php

namespace App\Jobs\Joost\Importer\Model;

/**
 * Class XML
 *
 * @package App\Jobs\Joost\Importer\Model
 *
 * Process data from XML file format
 */
class DataReaderXML extends DataReader
{
	public function __construct( $resourceFileName, $cursorPosition = 0 )
	{
		$this->dataDelimiterOpen  = '<row>';
		$this->dataDelimiterClose = '</row>';
		parent::__construct( $resourceFileName, $cursorPosition );
	}

	/**
	 * Get the data from json file
	 *
	 * @return array|bool
	 */
	public function getData()
	{
		$data = parent::getData();

		if ( $data ) {
			$this->convertXmlToArray( $data[ 'data' ] );
			$this->fixArrayValuesToNull( $data[ 'data' ] );
		}

		return $data;
	}

	/**
	 * @param string $data
	 */
	private function convertXmlToArray( string &$data ) : void
	{
		// Replace <br> for <br/> so SimpleXMLElement don't try to find the closing tag
		$data = str_replace( '<br>', '<br/>', $data );
		// Replace the symbol & by &amp; so SimpleXMLElement can be able to import the string without problems
		$data = str_replace( '&', '&amp;', $data );

		// Convert string to XML
		$data = new \SimpleXMLElement( $data );
		// Convert XML to JSON - Necessary, cause json_decode is not able to convert XML to Array
		$data = json_encode( $data );
		// Convert JSON to Array
		$data = json_decode( $data, true );
	}

	/**
	 * @param array $data
	 */
	private function fixArrayValuesToNull( array &$data ) : void
	{
		// Fix some conversions problems - Convert empty arrays to null
		$data = array_map( function ( $item ) {
			if ( is_array( $item ) && count( $item ) === 0 ) {
				$item = null;
			}

			return $item;
		}, $data );
	}

}