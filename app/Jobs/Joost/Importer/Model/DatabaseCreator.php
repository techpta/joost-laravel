<?php

namespace App\Jobs\Joost\Importer\Model;

class DatabaseCreator
{

	/** @var $cnx \PDO */
	private $cnx;

	public function __construct( \PDO $cnx )
	{
		$this->cnx = $cnx;
	}

	public function __invoke()
	{
		$this->createDatabase();
		$this->setEnvironment( 'start' );
		$this->createTableImporterFiles();
		$this->createTableProfiles();
		$this->createTableProfilesAddresses();
		$this->createTableProfilesEmails();
		$this->createTableCreditCards();
		$this->setEnvironment( 'end' );
	}

	private function createDatabase() : void
	{
		$this->cnx->exec( 'CREATE DATABASE joost' );
		$this->cnx->exec( 'USE joost' );
	}

	private function setEnvironment( $step ) : void
	{
		$cmd = $step === 'start'
			? 'SET NAMES utf8mb4; SET FOREIGN_KEY_CHECKS = 0;'
			: 'SET FOREIGN_KEY_CHECKS = 1;';

		$this->cnx->exec( $cmd );
	}

	private function createTableCreditCards() : void
	{
		$this->cnx->exec( '
		DROP TABLE IF EXISTS `credit_cards`;
		CREATE TABLE `credit_cards`  (
		  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `type` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  `name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  `number` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  `expirationDate` CHAR(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  PRIMARY KEY (`id`) USING BTREE
		) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;
		' );
	}

	private function createTableImporterFiles() : void
	{
		$this->cnx->exec( '
		DROP TABLE IF EXISTS `importerfiles`;
		CREATE TABLE `importerfiles`  (
		  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `started` TIMESTAMP(0) NULL DEFAULT CURRENT_TIMESTAMP,
		  `ended` TIMESTAMP(0) NULL DEFAULT NULL,
		  `resourceFileName` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  `cursorPosition` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
		  PRIMARY KEY (`id`) USING BTREE,
		  INDEX `currentFile`(`resourceFileName`) USING BTREE
		) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;
		' );
	}

	private function createTableProfiles() : void
	{
		$this->cnx->exec( '
		DROP TABLE IF EXISTS `profiles`;
		CREATE TABLE `profiles`  (
		  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  `address` INT(10) UNSIGNED NULL DEFAULT NULL,
		  `checked` ENUM(\'Yes\',\'No\') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT \'No\',
		  `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
		  `interest` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  `date_of_birth` DATE NULL DEFAULT NULL,
		  `account` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
		  `email` INT(10) UNSIGNED NULL DEFAULT NULL,
		  `credit_card` INT(10) UNSIGNED NULL DEFAULT NULL,
		  PRIMARY KEY (`id`) USING BTREE,
		  INDEX `email`(`email`) USING BTREE,
		  INDEX `credit_card`(`credit_card`) USING BTREE,
		  INDEX `address`(`address`) USING BTREE,
		  CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`address`) REFERENCES `profiles_addresses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
		  CONSTRAINT `profiles_ibfk_2` FOREIGN KEY (`email`) REFERENCES `profiles_emails` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
		  CONSTRAINT `profiles_ibfk_3` FOREIGN KEY (`credit_card`) REFERENCES `credit_cards` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
		) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;
		' );
	}

	private function createTableProfilesAddresses() : void
	{
		$this->cnx->exec( '
			DROP TABLE IF EXISTS `profiles_addresses`;
			CREATE TABLE `profiles_addresses`  (
			  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			  `address` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
			  PRIMARY KEY (`id`) USING BTREE
			) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;
		' );
	}

	private function createTableProfilesEmails() : void
	{
		$this->cnx->exec( '
			DROP TABLE IF EXISTS `profiles_emails`;
			CREATE TABLE `profiles_emails`  (
			  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			  `email` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
			  PRIMARY KEY (`id`) USING BTREE
			) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;
		' );
	}

}