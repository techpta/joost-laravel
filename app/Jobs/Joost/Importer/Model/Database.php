<?php

namespace App\Jobs\Joost\Importer\Model;

final class Database
{

	/** @var $cnx \PDO */
	private static $cnx;

	public function __construct()
	{
		static::$cnx = static::getConnection();
	}

	/**
	 * Connection with the database
	 *
	 * @return \PDO
	 */
	public static function getConnection()
	{
		if ( null === static::$cnx )

			try {
				static::$cnx = new \PDO( 'mysql:host=127.0.0.1;charset=utf8;', 'root', '',
					[
						\PDO::ATTR_EMULATE_PREPARES => false,
						\PDO::ATTR_ERRMODE          => \PDO::ERRMODE_EXCEPTION
					]
				);
				static::$cnx->exec( 'USE joost' );
			} catch ( \PDOException $exception ) {
				$dbc = new DatabaseCreator( static::$cnx );
				$dbc();
				$dbc = null;
			}

		return static::$cnx;
	}

	/**
	 * @param string $table
	 * @param array  $fields
	 * @param array  $data
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function insertData( string $table, array $fields, array $data )
	{
		$mask   = substr( str_repeat( '?, ', count( $fields ) ), 0, -2 );
		$fields = implode( ', ', $fields );

		$stmt    = static::$cnx->prepare( "INSERT INTO $table ( $fields ) VALUE ( $mask )" );
		$success = $stmt->execute( $data );
		if ( !$success ) {
			if ( static::$cnx->inTransaction() ) {
				static::$cnx->rollBack();
			}
			throw new \Exception( static::$cnx->errorInfo()[ 2 ] );
		}

		return static::$cnx->lastInsertId();
	}

	/**
	 * Closes the connection with database
	 */
	public function __destruct()
	{
		static::$cnx = null;
	}

}