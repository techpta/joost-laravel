<?php

namespace App\Jobs\Joost\Importer\Model;

/**
 * Class JSON
 *
 * @package App\Jobs\Joost\Importer\Model
 *
 * Process data from JSON file format
 */
class DataReaderJSON extends DataReader
{

	public function __construct( $resourceFileName, $cursorPosition = 0 )
	{
		$this->dataDelimiterOpen  = '{"name"';
		$this->dataDelimiterClose = '}}';
		parent::__construct( $resourceFileName, $cursorPosition );
	}

	/**
	 * Get the data from json file
	 *
	 * @return array|bool
	 */
	public function getData()
	{
		$data = parent::getData();

		if ( $data ) {
			$data[ 'data' ] = json_decode( $data[ 'data' ], true );
		}

		return $data;
	}
}