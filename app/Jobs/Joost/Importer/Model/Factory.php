<?php

namespace App\Jobs\Joost\Importer\Model;

/**
 * Class Factory
 *
 * @package App\Jobs\Joost\Importer\Model
 *
 * This class create new instances of other classes. ( It's a Design Pattern )
 */
class Factory
{

	/**
	 * @param string $resourceFileName
	 * @param int    $cursorPosition
	 *
	 * @return DataReaderJSON
	 */
	public static function createJSONDataReader( string $resourceFileName, int $cursorPosition = 0 )
	{
		return new DataReaderJSON( $resourceFileName, $cursorPosition );
	}

	/**
	 * @param string $resourceFileName
	 * @param int    $cursorPosition
	 *
	 * @return DataReaderXML
	 */
	public static function createXMLDataReader( string $resourceFileName, int $cursorPosition = 0 )
	{
		return new DataReaderXML( $resourceFileName, $cursorPosition );
	}

}