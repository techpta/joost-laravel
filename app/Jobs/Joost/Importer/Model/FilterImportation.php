<?php

namespace App\Jobs\Joost\Importer\Model;

class FilterImportation
{

	/**
	 * For now, the allowed age parameters are:
	 * 1. No age informed
	 * 2. Age between 18 and 65
	 *
	 * @param $data
	 *
	 * @return bool
	 */
	public static function isAgeValidToImport( array $data ) : bool
	{
		$index         = array_search( 'date_of_birth', $data[ 'profile' ][ 'fields' ] );
		$date_of_birth = $data[ 'profile' ][ 'data' ][ $index ];
		$age           = static::calculateAge( $date_of_birth );

		if ( empty( $date_of_birth ) ) return true;
		if ( $age >= 18 && $age <= 65 ) return true;

		return false;
	}

	/**
	 * Accepts a date of birth in 'yyyy-mm-dd' string format and return the current age
	 *
	 * @param $date_of_birth
	 *
	 * @return float
	 */
	public static function calculateAge( $date_of_birth ) : float
	{
		$date  = new \DateTime( $date_of_birth );
		$today = new \DateTime( date( 'Y-m-d' ) );
		$diff  = $today->diff( $date );

		return (float)round( $diff->format( '%a' ) / 365, 2 );
	}

	/**
	 * Currently this filter is not being use, but it is part of the Bonus of challenge.
	 * It verifies if there is a sequence of three identical numbers in credit card number
	 *
	 * @param array $data
	 *
	 * @return bool
	 */
	public static function isCreditCardValidToImport( array $data ) : bool
	{
		$index              = array_search( 'number', $data[ 'credit_card' ][ 'fields' ] );
		$credit_card_number = $data[ 'credit_card' ][ 'data' ][ $index ];

		if ( preg_match( '/(\d)\1{2}/', $credit_card_number ) === 1 ) {
			return true;
		}

		return false;
	}

}