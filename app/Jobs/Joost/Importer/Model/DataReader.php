<?php

namespace App\Jobs\Joost\Importer\Model;

/**
 * Class DataReader
 *
 * @package App\Jobs\Joost\Importer
 *
 * Abstract base class for read data from various types of files
 */
abstract class DataReader
{

	protected
		$dataDelimiterOpen,
		$dataDelimiterClose;

	private
		$resource,
		$resourceFileName,
		$resourceFileSize,
		$cursor,
		$cursorStartedAtZero,
		$chunkSize,
		$chunk;

	public function __construct( string $resourceFileName, int $cursorPosition = 0 )
	{
		$resourceFileName = __DIR__ . '/' . $resourceFileName;
		if ( !file_exists( $resourceFileName ) ) throw new \Exception( 'File [' . $resourceFileName . '] not found.' );

		$this->resourceFileName    = $resourceFileName;
		$this->resourceFileSize    = filesize( $resourceFileName );
		$this->resource            = fopen( $resourceFileName, 'r' );
		$this->chunkSize           = 100;
		$this->cursor              = $cursorPosition;
		$this->cursorStartedAtZero = $cursorPosition === 0;
	}

	/**
	 * Process the chunk to get the data between the Delimiters
	 *
	 * @return array|bool
	 */
	public function getData()
	{
		$this->chunk = '';
		$this->getChunk();

		if ( feof( $this->resource ) ) return false;

		$start = $this->getDelimiterPosition( $this->dataDelimiterOpen );
		$end   = $this->getDelimiterPosition( $this->dataDelimiterClose, $start );

		# Rewind the cursor to beginning of the next data (It may have over read data)
		$this->cursor -= strlen( $this->chunk ) - $end - 1;

		# Convert the Close Delimiter position to the length of the chunk to be recovered
		$length = $end - $start + strlen( $this->dataDelimiterClose );

		return [
			'data'                => substr( $this->chunk, $start, $length ),
			'nextDataAtCursorPos' => $this->cursor
		];
	}

	/**
	 * Get the chunk in resource based in cursor position and stores it in the $this->chunk variable
	 */
	private function getChunk()
	{
		fseek( $this->resource, $this->cursor );
		$this->cursor += $this->chunkSize;
		$this->chunk .= fread( $this->resource, $this->chunkSize );
	}

	/**
	 * Auxiliar method to find the position of a delimiter
	 *
	 * @param     $delimiter
	 * @param int $from
	 *
	 * @return bool|int
	 */
	private function getDelimiterPosition( $delimiter, $from = 0 )
	{
		do {
			$position = strpos( $this->chunk, $delimiter, $from );
			if ( $position === false ) $this->getChunk();
		} while ( $position === false );

		return $position;
	}

	/**
	 * Recovery the name of the file that is being read
	 *
	 * @return string
	 */
	public function getResourceFileName() : string
	{
		return $this->resourceFileName;
	}

	/**
	 * Inform the percentage of the cursor position in relation to the end of the file
	 *
	 * @return string
	 */
	public function getProgress() : string
	{
		return round( $this->cursor / $this->resourceFileSize * 100, 2 ) . '%';
	}

	/**
	 * Change the cursor position for the next data search
	 *
	 * @param int $position
	 */
	public function setCursorTo( int $position )
	{
		$this->cursor = $position;
	}

	/**
	 * Closes resource
	 */
	public function __destruct()
	{
		fclose( $this->resource );
	}

}
