<?php

namespace App\Jobs\Joost\Importer\Model;

class Importation
{

	/**
	 * @var $cnx \PDO
	 */
	private $cnx;
	/**
	 * @var $db Database
	 */
	private $db;
	/**
	 * @var $processing_id int
	 */
	private $processing_id;
	/**
	 * @var $resourceFileName string
	 */
	private $resourceFileName;

	public function __construct( Database $db, string $resourceFileName )
	{
		$this->db               = $db;
		$this->cnx              = $db->getConnection();
		$this->resourceFileName = $resourceFileName;
	}

	/**
	 * Proccess the data, filter it, import to database and update the progress of importation
	 *
	 * @param array $source
	 *
	 * @return bool
	 */
	public function importData( array $source )
	{
		$data = $this->prepareData( $source[ 'data' ] );

		if ( !$this->isDataAllowedToImport( $data ) ) {
			return false;
		}

		$profile     = $data[ 'profile' ];
		$email       = $data[ 'email' ];
		$address     = $data[ 'address' ];
		$credit_card = $data[ 'credit_card' ];

		if ( !$this->isAlreadyImported( 'profiles', $profile[ 'fields' ], $profile[ 'data' ] ) ) {
			$this->cnx->beginTransaction();
			$profile_id = $this->db->insertData( 'profiles', $profile[ 'fields' ], $profile[ 'data' ] );

			$address_id     = $this->importAddress( $address );
			$email_id       = $this->importEmail( $email );
			$credit_card_id = $this->importCreditCard( $credit_card );

			$this->updateProfile( $profile_id, $address_id, $email_id, $credit_card_id );
			$this->saveProgress( $source[ 'nextDataAtCursorPos' ] );
			$this->cnx->commit();
		}

		return true;
	}

	/**
	 * Separete the given data in a way that it can be stored in each respective table
	 *
	 * @param $source
	 *
	 * @return array
	 */
	private function prepareData( $source )
	{
		$profile[ 'fields' ] = array_keys( $source );
		$profile[ 'data' ]   = array_values( $source );

		$profile = $this->formatData( $profile );

		$credit_card[ 'fields' ] = array_keys( $source[ 'credit_card' ] );
		$credit_card[ 'data' ]   = array_values( $source[ 'credit_card' ] );

		$address = $profile[ 'data' ][ array_search( 'address', $profile[ 'fields' ] ) ];
		$email   = $profile[ 'data' ][ array_search( 'email', $profile[ 'fields' ] ) ];

		// Remove address, email and credit_card from profile, because it will be inserted in separated table and the ID will be updated in profiles' table later
		$profile = $this->removeItemsFromProfile( $profile );

		return [
			'profile'     => $profile,
			'address'     => $address,
			'email'       => $email,
			'credit_card' => $credit_card
		];
	}

	/**
	 * Format the 'checked' and 'date_of_birth' fields for database insertion
	 *
	 * @param array $profile
	 *
	 * @return array
	 */
	private function formatData( array $profile )
	{
		$checked       = array_search( 'checked', $profile[ 'fields' ] );
		$date_of_birth = array_search( 'date_of_birth', $profile[ 'fields' ] );

		$profile[ 'data' ][ $checked ]       = DataStandardization::checked( $profile[ 'data' ][ $checked ] );
		$profile[ 'data' ][ $date_of_birth ] = DataStandardization::date( $profile[ 'data' ][ $date_of_birth ] );

		return $profile;
	}

	private function removeItemsFromProfile( array $profile )
	{
		$email = array_search( 'email', $profile[ 'fields' ] );
		array_splice( $profile[ 'fields' ], $email, 1 );

		$address = array_search( 'address', $profile[ 'fields' ] );
		array_splice( $profile[ 'fields' ], $address, 1 );

		$credit_card = array_search( 'credit_card', $profile[ 'fields' ] );
		array_splice( $profile[ 'fields' ], $credit_card, 1 );

		array_splice( $profile[ 'data' ], $email, 1 );
		array_splice( $profile[ 'data' ], $address, 1 );
		array_splice( $profile[ 'data' ], $credit_card, 1 );

		return $profile;
	}

	/**
	 * Verify if $data matches the criteria for importation
	 *
	 * @param array $data
	 *
	 * @return bool
	 */
	private function isDataAllowedToImport( array $data ) : bool
	{
		/*
		 * Since there is only one filter active, I could write 'return FilterImportation::isAgeValidToImport( $data );'
		 * But to leave a 'template' of code for more filters, I leave the IF(...) return false
		 * So if no filter rules out the data, in the end it returns true and data can be imported
		 */

		if ( !FilterImportation::isAgeValidToImport( $data ) ) return false;

		// DISABLED, but this is part of the Bonus challenge where accepts only records with credit card having a sequence of three identical numbers
		// if ( !FilterImportation::isCreditCardValidToImport( $data ) ) return false;

		return true;
	}

	/**
	 * Verify if the data is already imported to database
	 *
	 * Returns boolean false if not and integer ID of the record if already imported.
	 *
	 * @param string $table
	 * @param array  $fields
	 * @param array  $data
	 *
	 * @return bool|int
	 */
	private function isAlreadyImported( string $table, array $fields, array $data )
	{
		$where = implode( '=? AND ', $fields ) . '=?';

		list( $where, $data ) = $this->fixNullValuesForQuery( $fields, $data, $where );

		$stmt = $this->cnx->prepare( "SELECT id FROM $table WHERE $where LIMIT 1" );
		$stmt->execute( $data );

		return $stmt->rowCount() > 0
			? $stmt->fetch( \PDO::FETCH_COLUMN )
			: false;
	}

	/**
	 * Receives the $fields, $data and $where, look in $data for null values and replace in $where the name of $field from
	 * the base format 'field=null' to 'field IS NULL', so the query can work properly in database
	 *
	 * @param $fields
	 * @param $data
	 * @param $where
	 *
	 * @return array
	 */
	private function fixNullValuesForQuery( $fields, $data, $where ) : array
	{
		$total = count( $fields );
		// $pos is the same index of for loop $i, but every array_splice in $data, makes $data smaller, so we need to adapt the index, to it works
		$pos = 0;
		for ( $i = 0; $i < $total; $i++ ) {
			if ( $data[ $pos ] === null ) {
				array_splice( $data, $pos, 1 );
				// Since "field=null" does not work, we need to change it to 'field IS NULL'
				$where = str_replace( $fields[ $i ] . '=?', $fields[ $i ] . ' IS NULL', $where );
				// Fix the index after removing an element from array $data
				$pos--;
			}
			// Keep $pos incrementing just like $i from for loop
			$pos++;
		}

		return [ $where, $data ];
	}

	/**
	 * Import address if not imported already
	 *
	 * @param $address
	 *
	 * @return bool|int|string
	 */
	private function importAddress( $address )
	{
		$address_id = $this->isAlreadyImported( 'profiles_addresses', [ 'address' ], [ $address ] );
		if ( $address_id === false ) {
			$address_id = $this->db->insertData( 'profiles_addresses', [ 'address' ], [ $address ] );
		}

		return $address_id;
	}

	/**
	 * Import email if not imported already
	 *
	 * @param $email
	 *
	 * @return bool|int|string
	 */
	private function importEmail( $email )
	{
		$email_id = $this->isAlreadyImported( 'profiles_emails', [ 'email' ], [ $email ] );
		if ( $email_id === false ) {
			$email_id = $this->db->insertData( 'profiles_emails', [ 'email' ], [ $email ] );
		}

		return $email_id;
	}

	/**
	 * Import credit card if not imported already
	 *
	 * @param $credit_card
	 *
	 * @return bool|int|string
	 */
	private function importCreditCard( $credit_card )
	{
		$credit_card_id = $this->isAlreadyImported( 'credit_cards', $credit_card[ 'fields' ], $credit_card[ 'data' ] );
		if ( $credit_card_id === false ) {
			$credit_card_id = $this->db->insertData( 'credit_cards', $credit_card[ 'fields' ], $credit_card[ 'data' ] );
		}

		return $credit_card_id;
	}

	/**
	 * Update the profile record with the Foreign Key of the address and the email
	 *
	 * @param int $profile_id
	 * @param int $address_id
	 * @param int $email_id
	 * @param int $credit_card_id
	 */
	private function updateProfile( int $profile_id, int $address_id, int $email_id, int $credit_card_id )
	{
		if ( empty( $address_id ) ) $address_id = 'null';
		if ( empty( $email_id ) ) $email_id = 'null';
		if ( empty( $credit_card_id ) ) $credit_card_id = 'null';

		$this->cnx->exec( "UPDATE profiles SET address=$address_id, email=$email_id, credit_card=$credit_card_id WHERE id=$profile_id LIMIT 1" );
	}

	/**
	 * When saving by first time, it creates the record, otherwise it updates the cursorPosition
	 *
	 * @param int  $cursorPosition
	 * @param bool $finished
	 *
	 * @throws \Exception
	 */
	public function saveProgress( int $cursorPosition = 0, $finished = false ) : void
	{
		if ( null === $this->processing_id ) {
			$stmt = $this->cnx->prepare( "SELECT id FROM importerfiles WHERE resourceFileName=? AND ended IS NULL LIMIT 1" );
			$stmt->execute( [ $this->resourceFileName ] );
			if ( $stmt->rowCount() > 0 ) {
				$this->processing_id = $stmt->fetch( \PDO::FETCH_COLUMN );
			}
		}

		if ( $finished ) {
			$stmt    = $this->cnx->prepare( "UPDATE importerfiles SET ended=? WHERE id=? LIMIT 1" );
			$success = $stmt->execute( [ date( 'Y-m-d H:i:s' ), $this->processing_id ] );
		} else {
			if ( $this->processing_id ) {
				$stmt    = $this->cnx->prepare( "UPDATE importerfiles SET cursorPosition=? WHERE id=? LIMIT 1" );
				$success = $stmt->execute( [ $cursorPosition, $this->processing_id ] );
			} else {
				$stmt    = $this->cnx->prepare( "INSERT INTO importerfiles (id, started, resourceFileName, cursorPosition) VALUES (?, ?, ?, ?)" );
				$success = $stmt->execute( [ $this->processing_id, date( 'Y-m-d H:i:s' ), $this->resourceFileName, $cursorPosition ] );
			}
		}

		if ( !$success ) {
			if ( $this->cnx->inTransaction() ) {
				$this->cnx->rollBack();
			}
			throw new \Exception( 'It was not possible to save the progress of importation.' );
		}

		// If it is an insert and not an update, it recovers the created ID
		if ( null === $this->processing_id ) {
			$this->processing_id = $this->cnx->lastInsertId();
		}
	}

	/**
	 * Search for any importation that was interrupted and return it's information to continue the process
	 *
	 * @return bool|array
	 */
	public function getPendingImportation()
	{
		$importation = $this->cnx->query( "SELECT id, resourceFileName, cursorPosition FROM importerfiles WHERE ended IS NULL LIMIT 1" );

		$importation = $importation->rowCount() > 0
			? $importation->fetch( \PDO::FETCH_ASSOC )
			: false;

		if ( $importation ) {
			$this->processing_id    = $importation[ 'id' ];
			$this->resourceFileName = $importation[ 'resourceFileName' ];
		}

		return $importation;
	}
}