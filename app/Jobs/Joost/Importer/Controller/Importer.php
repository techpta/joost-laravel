<?php

namespace App\Jobs\Joost\Importer\Controller;

use App\Jobs\Joost\Importer\Model\Database;
use App\Jobs\Joost\Importer\Model\Factory;
use App\Jobs\Joost\Importer\Model\Importation;

class Importer
{
	private $dataReader;
	private $importation;

	public function __construct(
		string $resourceFileName = '/../../uploaded/data.json',
		int $cursorPosition = 0
	)
	{
		$this->importation = new Importation( new Database(), $resourceFileName );

		$extension = strrchr( strtoupper( $resourceFileName ), '.' );
		switch ( $extension ) {
			case '.XML':
				$this->dataReader = Factory::createXMLDataReader( $resourceFileName, $cursorPosition );
				break;
			default:
				$this->dataReader = Factory::createJSONDataReader( $resourceFileName, $cursorPosition );
		}
	}

	/**
	 * This method is used by front-end for entertainment purpose only
	 *
	 * @return array|bool
	 * @throws \Exception
	 */
	public function importNextData()
	{
		$data = $this->dataReader->getData();

		// Create the register of file being processed
		$this->importation->saveProgress( $data[ 'nextDataAtCursorPos' ] );


		if ( $data ) {
			$this->importation->importData( $data );
		} else {
			// Set timestamp of importation in field 'ended' so we know this file was fully processed
			$this->importation->saveProgress( 0, true );
		}

		return $data;
	}

	/**
	 * If is there any importation pending, continue.
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function resumeImportation() : bool
	{
		$pendingImportation = $this->importation->getPendingImportation();
		if ( $pendingImportation ) {
			$this->dataReader->setCursorTo( $pendingImportation[ 'cursorPosition' ] );
		}

		return $this->importData();
	}

	/**
	 * Import all data at once
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function importData() : bool
	{
		/*  Since in php.ini the max_execution_time is 30 seconds (By default) and the process can take longer, we need to extend it here.
		 *  CAUTION !!!!
		 *  0 = Means no time limit to run, but this can create zombie process, not recommended in production.
		 */
		set_time_limit( 0 );

		while ( $data = $this->dataReader->getData() ) {
			// Create and register the file being processed
			$this->importation->saveProgress( $data[ 'nextDataAtCursorPos' ] );
			$this->importation->importData( $data );
		}

		// Set the timestamp of ended field in table importerfiles, so we know it was finished the job
		$this->importation->saveProgress( 0, true );

		// Just to know when it ends, on screen.
		echo 'End of importation';

		return false;
	}

	/**
	 * Get the porcentagem progress of importation based on cursor position
	 *
	 * @return string
	 */
	public function getProgress()
	{
		return $this->dataReader->getProgress();
	}
}
