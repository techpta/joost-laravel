<?php

namespace App\Http\Controllers;

use App\Jobs\ImportJson;
use App\Jobs\ImportXml;
use Illuminate\Http\Request;

class ImporterController extends Controller
{
	public function importJson()
	{
		$this->dispatch( new ImportJson() );
	}

	public function importXml()
	{
		$this->dispatch( new ImportXml() );
	}
}
